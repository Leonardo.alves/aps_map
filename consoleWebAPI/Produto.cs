
namespace consoleWebAPI
{
    public class Produto
    {
        public int idProd {get; set;}
        public string nomeProd {get; set;}
        public string descProd {get; set;}

        public override string ToString()
        {
            return string.Format($"{nomeProd} - {descProd}");
        }
    }
}