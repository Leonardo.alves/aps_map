﻿using System;
using System.Threading.Tasks;
using static System.Console;

namespace consoleWebAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Acessando a web API, aguarde um momento...");

            var repositorio = new ProdutoRepositorio();

            var produtosTask = repositorio.GetProdutoAsync(); 

            produtosTask.ContinueWith(task =>
            {
                var produtos = produtosTask.Result;
                foreach (var prod in produtos)
                    WriteLine(prod.ToString());
                Environment.Exit(0);
            },
            TaskContinuationOptions.OnlyOnRanToCompletion
            );   
            ReadLine();
        }
    }
}
