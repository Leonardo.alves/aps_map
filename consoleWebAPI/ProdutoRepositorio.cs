using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace consoleWebAPI
{
    public class ProdutoRepositorio
    {
        HttpClient cliente = new HttpClient();

        public ProdutoRepositorio()
        {
            cliente.BaseAddress = new Uri("http://localhost:5000/");

            cliente.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Produto>> GetProdutoAsync()
        {
            HttpResponseMessage responde = await cliente.GetAsync("api/produto");
            if (responde.IsSuccessStatusCode)
            {
                var dados = await responde.Content.ReadAsStringAsync();
                return JsonConverter.DeserializeObject<List<Produto>>(dados);
            }
            return new List<Produto>();
        }
    }

}