using System.Collections.Generic;
using apsmap.Models;

namespace apsmap.Repositorio
{
    public interface IProdutoRepositorio
    {
        IEnumerable<Produto> GetAll();
        Produto Get(int id);
        Produto Add(Produto prod);
        void Remove(int id);
        void Update(Produto prod);
    }
}