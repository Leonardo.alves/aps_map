
using System.Collections.Generic;
using System.Linq;
using apsmap.Models;
using apsmap.Repositorio;

namespace WebApi.Models
{    
    
    public class ProdutoRepositorio : IProdutoRepositorio
    {
        private readonly ProdutoDBContext _contexto;

        public ProdutoRepositorio(ProdutoDBContext _ctx)
        {
            _contexto = ctx;
        }
        public Produto Add(Produto prod)
        {
            _contexto.Produto.Add(prod);
            _contexto.SaveChanges();
        }
        public Produto Get(int id)
        {
            return _contexto.Produto.FirstOrDefault(p => p.idProd == id);
        }
        public IEnumerable<Produto> GetAll()
        {
            return _contexto.Produto.ToList();
        }
        public void Remove(int id)
        {
            var entity = _contexto.Produto.First(p => p.idProd == id);
            _contexto.Produto.Remove(entity);
            _contexto.SaveChanges();
        }
        public bool Update(Produto prod)
        {
            _contexto.Produto.Update(prod);
            _contexto.SaveChanges();
        }

        void IProdutoRepositorio.Update(Produto prod)
        {
            throw new System.NotImplementedException();
        }
    }
}