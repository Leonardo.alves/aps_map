using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using apsmap.Models;
using apsmap.Repositorio;

namespace apsmap.Controllers
{
    [Route("api/[Controller]")]
    public class ProdutoController : Controller
    {
        public ProdutoController (IProdutoRepositorio prodRep)
        {
            _produtoRepositorio = prodRep;
        }
        [HttpGet]
        public IEnumerable<Produto> GetAll()
        {
            return _produtoRepositorio.GetAll();
        }

        [HttpGet("{id}", Name="GetProjeto")]
        public IActionResult GetById(int id)
        {
            var produto = _produtoRepositorio.Find(id);
            if(produto==null)
                return NotFound();

            return new ObjectResult(produto);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Produto produto)
        {
            if(produto==null)
                return BadRequest();

            _produtoRepositorio.Add(produto);

            return CreatedAtRoute("GetProduto", new{id=produto}, produto);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Produto produto)
        {
            if(produto==null || produto.idProd !=id)
                return BadRequest();

            var _produto = _produtoRepositorio.Find(id);

            if(_produto==null)
                return NotFound();

            _produto.nomeProd = produto.nomeProd;
            _produto.descProd = produto.descProd; 

            _produtoRepositorio.Update(_produto);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete (int id)
        {
            var produto = _produtoRepositorio.Find(id);
            if(_produto==null)
                return NotFound();

            _produtoRepositorio.Remove(id);
            return new NoContentResult();
        }
    }
}