using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace apsmap.Models
{
    public class ProdutoDBContext : DbContext
    {
        public ProdutoDBContext (DbContextOptions<ProdutoDBContext>options)
            : base (options)
        {}
        public DbSet<Produto> Produto {get; set;}
    }
}