﻿CREATE TABLE [dbo].[Produto] (
    [ID]    INT            NOT NULL,
    [Nome]  VARCHAR (50)   NULL,
    [Categoria]  NVARCHAR (MAX) NULL,
    [Preco] DECIMAL (18)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

